# Generic Auto-Update Script
`Codename: Stork`  
[`Current Version: 2.0.2`](https://gitlab.com/NuclearEagleFox/example/tags/v2.0.2)

A script that pulls the latest version of a Git repository. Built using Python and Git.

### Building & Running
```
autoupdate
```

### Status & Contribution
At this time, I consider this project inactive. However, I am developing it for my own personal use. Forks are therefore preferred to pull requests.

### Roadmap
A guide to planned features and future versions.

Currently, there are no planned features

### Related Project(s)
https://gitlab.com/NuclearEagleFox/trilobite
